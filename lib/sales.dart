import 'package:construccion/custom_widgets/build_appbar.dart';
import 'package:construccion/includes/styles.dart';
import 'package:construccion/utils/database_helper.dart';
import 'package:flutter/material.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:sqflite/sqflite.dart';

import 'custom_widgets/build_listview_search.dart';
import 'custom_widgets/sidebar.dart';
import 'includes/list_item.dart';
import 'models/sale.dart';
import 'package:path/path.dart' as path;

class SalesPage extends StatefulWidget {
  @override
  _SalesPageState createState() => new _SalesPageState();
}

class _SalesPageState extends State<SalesPage> {
  int _bottomNavIndex = 0;

  DatabaseHelper helper = DatabaseHelper();
  Future<SqfliteAdapter> adapter = helper.getAdapter();

  List<ListItem> items = [
    ListItem("1", "6354", "Inducesmile.com", "subtitle"),
    ListItem("1", "6354", "Flutter Dev", "subtitle"),
    ListItem("1", "6354", "Android Dev", "subtitle"),
    ListItem("1", "6354", "iOS Dev!", "subtitle"),
    ListItem("1", "6354", "React Native Dev!", "subtitle"),
    ListItem("1", "6354", "React Dev!", "subtitle"),
    ListItem("1", "6354", "express Dev!", "subtitle"),
    ListItem("1", "6354", "Laravel Dev!", "subtitle"),
    ListItem("1", "6354", "Angular Dev!", "subtitle"),
  ];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Styles.primaryColor,
        child: Icon(Icons.add),
        onPressed: () {},
      ),
      bottomNavigationBar: new BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        fixedColor: Color(0XFF29D091),
        currentIndex: _bottomNavIndex,
        onTap: (int index) {
          setState(() {
            _bottomNavIndex = index;
          });
        },
        items: [
          new BottomNavigationBarItem(
              title: new Text(''), icon: new Icon(Icons.home)),
          new BottomNavigationBarItem(
              title: new Text(''), icon: new Icon(Icons.local_offer)),
          new BottomNavigationBarItem(
              title: new Text(''), icon: new Icon(Icons.message)),
          new BottomNavigationBarItem(
              title: new Text(''), icon: new Icon(Icons.notifications))
        ],
      ),
      appBar: BuildAppBar(
        title: "Home",
      ),
      drawer: SideBar(),
      body: ListViewSearch(
        items: items,
      ),
    );
  }
}
