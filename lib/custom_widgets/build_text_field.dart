import 'package:flutter/material.dart';
import 'package:construccion/includes/styles.dart';

class BuildTextField extends StatefulWidget {
  final String labelText;
  final String hintText;
  final TextInputType textInputType;
  final TextEditingController controller;
  final String Function(String) validator;
  final bool obscureText;

  BuildTextField(
      {@required this.labelText,
      this.hintText = "",
      this.textInputType = TextInputType.text,
      @required this.controller,
      @required this.validator,
      this.obscureText = false});

  @override
  _BuildTextFieldState createState() => _BuildTextFieldState(
      labelText: this.labelText,
      hintText: this.hintText,
      textInputType: this.textInputType,
      controller: this.controller,
      validator: this.validator,
      obscureText: obscureText);
}

class _BuildTextFieldState extends State<BuildTextField> {
  Styles styles = Styles();

  String labelText;
  String hintText;
  TextInputType textInputType;
  TextEditingController controller;
  String Function(String) validator;
  bool obscureText = false;

  _BuildTextFieldState(
      {this.labelText,
      this.hintText,
      this.textInputType,
      this.controller,
      this.validator,
      this.obscureText});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: styles.textFieldPadding(),
      child: TextFormField(
        keyboardType: this.textInputType,
        obscureText: obscureText,
        // style: textStyle,
        controller: controller,
        validator: validator,
        decoration: InputDecoration(
          labelText: labelText,
          hintText: hintText,
          // labelStyle: textStyle,
          // errorStyle: errorStyle(),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
        ),
      ),
    );
  }
}
