import 'package:flutter/material.dart';

class SideBarOption extends StatefulWidget {

  final String text;
  final IconData icon;
  final Function onTab;

  SideBarOption({@required this.text, @required this.onTab, this.icon = Icons.account_balance});

  @override
  _SideBarOptionState createState() => _SideBarOptionState(text: text, onTab: onTab, icon: icon);
}

class _SideBarOptionState extends State<SideBarOption> {

  String text;
  IconData icon;
  Function onTab;

  _SideBarOptionState({this.text, this.onTab, this.icon});

  @override
  Widget build(BuildContext context) {
    return new ListTile(
      title: Text(
        text,
        style: TextStyle(color: Colors.black87),
      ),
      trailing: new Icon(icon),
      onTap: onTab,
    );
  }
}
