import 'package:flutter/material.dart';
import 'package:construccion/includes/styles.dart';

class BuildButton extends StatefulWidget {
  final Function onPressed;
  final String text;

  BuildButton({@required this.onPressed, @required this.text});

  @override
  _BuildButtonState createState() =>
      _BuildButtonState(onPressed: onPressed, text: text);
}

class _BuildButtonState extends State<BuildButton> {
  Styles styles = Styles();

  Function onPressed;
  String text;

  _BuildButtonState({this.onPressed, this.text});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: styles.buttonPadding(),
      child: RaisedButton(
        child: Text(
          text,
          style: styles.buttonTextStyle(),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
