import 'package:construccion/custom_widgets/build_listview_search.dart';
import 'package:construccion/custom_widgets/sidebar_option.dart';
import 'package:construccion/includes/navigate.dart';
import 'package:flutter/material.dart';

class SideBar extends StatelessWidget {

  final Navigate navigate = Navigate();

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: Text("APPTIVAWEB"),
            accountEmail: Text("informes@gmail.com"),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        "https://ichef.bbci.co.uk/news/660/cpsprodpb/6AFE/production/_102809372_machu.jpg"),
                    fit: BoxFit.cover)),
          ),
          SideBarOption(
            text: "Sales",
            icon: Icons.add_shopping_cart,
            onTab: () { navigate.toSalesPage(context); },
          ),
          SideBarOption(
            text: "MENU 2",
            icon: Icons.account_circle,
            onTab: () {},
          ),
          SideBarOption(
            text: "MENU 3",
            icon: Icons.account_circle,
            onTab: () {},
          ),
          SideBarOption(
            text: "MENU 4",
            icon: Icons.account_circle,
            onTab: () {},
          ),
          SideBarOption(
            text: "MENU 5",
            icon: Icons.account_circle,
            onTab: () {},
          ),
        ],
      ),
    );
  }
}
