import 'package:flutter/material.dart';
import 'package:construccion/includes/styles.dart';

class BuildIconButton extends StatefulWidget {
  final Function onPressed;
  final String text;
  final Color color;
  final Color iconColor;
  final double height;
  final IconData icon;
  final EdgeInsets padding;

  BuildIconButton(
      {@required this.onPressed,
      @required this.text,
      this.color = Styles.primaryColor,
      this.iconColor = Colors.white,
      this.height = Styles.buttonHeight,
      this.icon = Icons.accessible,
      this.padding = Styles.iconButtonPadding});

  @override
  _BuildIconButtonState createState() => _BuildIconButtonState(
      onPressed: onPressed,
      text: text,
      color: color,
      iconColor: iconColor,
      height: height,
      icon: icon,
      padding: padding);
}

class _BuildIconButtonState extends State<BuildIconButton> {
  Function onPressed;
  String text;
  Color color;
  Color iconColor;
  double height;
  IconData icon;
  EdgeInsets padding;

  _BuildIconButtonState(
      {this.onPressed,
      this.text,
      this.color,
      this.iconColor,
      this.height,
      this.icon,
      this.padding});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: Padding(
        padding: padding,
        child: FlatButton.icon(
          color: color,
          icon: Icon(
            icon,
            color: iconColor,
          ),
          label: Flexible(
            child: Text(
              text,
              maxLines: 2,
              overflow: TextOverflow.clip,
              style: TextStyle(fontSize: 13, color: Colors.white),
            ),
          ),
          onPressed: onPressed,
        ),
      ),
    );
  }
}
