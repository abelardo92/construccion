import 'package:construccion/includes/list_item.dart';
import 'package:flutter/material.dart';

class ListViewSearch extends StatefulWidget {
  final List<ListItem> items;
  ListViewSearch({@required this.items});
  _ListViewSearchState createState() => _ListViewSearchState(items: items);
}

class _ListViewSearchState extends State<ListViewSearch> {
  TextEditingController _textController = TextEditingController();

  List<ListItem> items;
  List<ListItem> _newData = [];

  _ListViewSearchState({this.items});

  _onChanged(String value) {
    setState(() {
      if (value.isEmpty) {
        _newData = items;
      } else {
        var newValue = value.toLowerCase();
        _newData = items
            .where((x) =>
                x.title.toLowerCase().contains(newValue) ||
                x.subtitle.toLowerCase().contains(newValue))
            .toList();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_textController.text.isEmpty) {
      _newData = items;
    }

    return Column(
      children: <Widget>[
        SizedBox(height: 0.0),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            controller: _textController,
            decoration: InputDecoration(
              hintText: 'enter text here',
            ),
            onChanged: _onChanged,
          ),
        ),
        buildListView(_newData),
      ],
    );
  }

  Expanded buildListView(List<ListItem> data) {
    return Expanded(
      child: ListView(
        padding: EdgeInsets.all(5.0),
        children: _newData.map((data) {
          return ListTile(
            title: Text(data.title),
            subtitle: Text(data.subtitle),
          );
        }).toList(),
      ),
    );
  }
}