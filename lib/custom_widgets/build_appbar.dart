import 'package:flutter/material.dart';
import 'package:construccion/includes/styles.dart';

class BuildAppBar extends StatefulWidget with PreferredSizeWidget {
  final String title;
  final Color backgroundColor;
  final Color iconColor;

  BuildAppBar(
      {@required this.title,
      this.backgroundColor = Styles.primaryColor,
      this.iconColor = Styles.primaryIconColor});
  @override
  _BuildAppBarState createState() => _BuildAppBarState(title: title, backgroundColor: backgroundColor, iconColor: iconColor);

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

class _BuildAppBarState extends State<BuildAppBar> {
  String title;
  Color backgroundColor;
  Color iconColor;
  _BuildAppBarState({this.title, this.backgroundColor, this.iconColor});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      // backgroundColor: Colors.transparent,
      // elevation: 0.0,
      title: Text(title),
      backgroundColor: backgroundColor,
      iconTheme: new IconThemeData(
        color: iconColor,
      ),
    );
  }
}
