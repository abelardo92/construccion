import 'package:construccion/custom_widgets/build_appbar.dart';
import 'package:construccion/custom_widgets/build_button.dart';
import 'package:construccion/custom_widgets/build_text_field.dart';
import 'package:construccion/includes/validators.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart'; 
import 'package:construccion/includes/navigate.dart';
import 'package:construccion/includes/styles.dart';

class LoginPage extends StatelessWidget {
  final Styles styles = Styles();
  final Validator validator = Validator();
  final Navigate navigate = Navigate();

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Styles styles = Styles();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.orange, //or set color with: Color(0xFF0000FF)
    ));

    return new Scaffold(
      appBar: BuildAppBar(
        title: "Login",
      ),
      resizeToAvoidBottomPadding: false,
      body: Container(
        width: double.infinity,
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            // new StakedIcons(),
            new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: styles.logoPadding(),
                  child: new Text(
                    "Quick Bee",
                    style: styles.logoTextStyle(),
                  ),
                )
              ],
            ),
            BuildTextField(
              labelText: "Email",
              hintText: "Example: john@domain.com",
              textInputType: TextInputType.emailAddress,
              controller: emailController,
              validator: validator.emailValidator(true),
              // obscureText: false,
            ),
            BuildTextField(
              labelText: "Password",
              hintText: "account password",
              controller: passwordController,
              validator: validator.passwordValidator(true),
              obscureText: true,
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: BuildButton(
                    text: "Login",
                    onPressed: () {
                      navigate.toHomePage(context);
                    },
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 10.0, right: 20.0, top: 10.0),
                    child: new Container(
                      alignment: Alignment.center,
                      height: 60.0,
                      child: new Text(
                        "Forgot Password?",
                        style: new TextStyle(
                          fontSize: 17.0,
                          color: Color(0xFF18D191),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 18.0),
                    child: new Text("Create A New Account ",
                        style: new TextStyle(
                            fontSize: 17.0,
                            color: Color(0xFF18D191),
                            fontWeight: FontWeight.bold)),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
