import 'package:construccion/models/customer.dart';
import 'package:construccion/models/product.dart';
import 'package:construccion/models/product_price.dart';
import 'package:construccion/models/sale.dart';
import 'package:construccion/models/sale_product.dart';
import 'package:construccion/models/sale_service.dart';
import 'package:construccion/models/service.dart';
import 'package:construccion/models/service_price.dart';
import 'package:construccion/models/user.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:path/path.dart' as path;
// import 'dart:io';
// import 'package:path_provider/path_provider.dart';

class DatabaseHelper {

/// The adapter
SqfliteAdapter _adapter;       // Singleton Database

	DatabaseHelper._createInstance(); // Named constructor to create instance of DatabaseHelper
  DatabaseHelper();
  // DatabaseHelper() {

  // }

	Future<SqfliteAdapter> getAdapter() async {

		var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "construction.db"));
    _adapter.connect();
    _createDb(_adapter, 1);
		return _adapter;
	}

	// Future<Database> get database async {

	// 	if (_database == null) {
	// 		_database = await initializeDatabase();
	// 	}
	// 	return _database;
	// }

	// Future<Database> initializeDatabase() async {
	// 	// // Get the directory path for both Android and iOS to store database.
	// 	// Directory directory = await getApplicationDocumentsDirectory();
	// 	// String path = directory.path + 'notes.db';

	// 	// Open/create the database at a given path
	// 	var database = await openDatabase(path, version: 1, onCreate: _createDb);
	// 	return database;
	// }

	void _createDb(SqfliteAdapter adapter, int newVersion) async {
    UserBean userBean = UserBean(adapter);
    ProductBean productBean = ProductBean(adapter);
    ProductPriceBean productPriceBean = ProductPriceBean(adapter);
    ServiceBean serviceBean = ServiceBean(adapter);
    ServicePriceBean servicePriceBean = ServicePriceBean(adapter);
    CustomerBean customerBean = CustomerBean(adapter);
    SaleBean saleBean = SaleBean(adapter);
    SaleProductBean saleProductBean = SaleProductBean(adapter);
    SaleServiceBean saleServiceBean = SaleServiceBean(adapter);

    userBean.createTable(ifNotExists: true);
    productBean.createTable(ifNotExists: true);
    productPriceBean.createTable(ifNotExists: true);
    serviceBean.createTable(ifNotExists: true);
    servicePriceBean.createTable(ifNotExists: true);
    customerBean.createTable(ifNotExists: true);
    saleBean.createTable(ifNotExists: true);
    saleProductBean.createTable(ifNotExists: true);
    saleServiceBean.createTable(ifNotExists: true);
	}
}