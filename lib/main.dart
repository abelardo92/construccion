import 'package:flutter/material.dart';
import 'package:construccion/includes/styles.dart';
import 'package:construccion/includes/navigate.dart';

import 'custom_widgets/build_button.dart';

void main() => runApp(QuickBee());

class QuickBee extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Styles styles = Styles();

    return MaterialApp(
      title: 'Quick Bee',
      debugShowCheckedModeBanner: false,
      // Set Raleway as the default app font
      theme: ThemeData(
          fontFamily: 'Roboto',
          buttonColor: Color(0xFF18D191),
          buttonTheme: styles.defaultButtonThemeData()),

      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  Navigate navigate = Navigate();
  Styles styles = Styles();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // logoIcons(),
            Padding(
              padding: styles.logoPadding(),
              child: titleText(),
            ),
            BuildButton(
              text: 'Sign in with Email',
              onPressed: () {
                navigate.toLoginPage(context);
              },
            ),
            // socialMediaButtons()
          ],
        ),
      ),
    );
  }

  Text titleText() {
    return new Text(
      "Quick Bee",
      style: styles.logoTextStyle(),
    );
  }

  Stack logoIcons() {
    return new Stack(
      alignment: Alignment.center,
      children: <Widget>[
        new Container(
          height: 60.0,
          width: 60.0,
          decoration: new BoxDecoration(
              borderRadius: new BorderRadius.circular(50.0),
              color: Color(0xFF18D191)),
          child: new Icon(
            Icons.local_offer,
            color: Colors.white,
          ),
        ),
        new Container(
          margin: new EdgeInsets.only(right: 50.0, top: 50.0),
          height: 60.0,
          width: 60.0,
          decoration: new BoxDecoration(
              borderRadius: new BorderRadius.circular(50.0),
              color: Color(0xFFFC6A7F)),
          child: new Icon(
            Icons.home,
            color: Colors.white,
          ),
        ),
        new Container(
          margin: new EdgeInsets.only(left: 30.0, top: 50.0),
          height: 60.0,
          width: 60.0,
          decoration: new BoxDecoration(
              borderRadius: new BorderRadius.circular(50.0),
              color: Color(0xFFFFCE56)),
          child: new Icon(
            Icons.local_car_wash,
            color: Colors.white,
          ),
        ),
        new Container(
          margin: new EdgeInsets.only(left: 90.0, top: 40.0),
          height: 60.0,
          width: 60.0,
          decoration: new BoxDecoration(
              borderRadius: new BorderRadius.circular(50.0),
              color: Color(0xFF45E0EC)),
          child: new Icon(
            Icons.place,
            color: Colors.white,
          ),
        )
      ],
    );
  }

  Row socialMediaButtons() {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 5.0, top: 10.0),
            child: new Container(
                alignment: Alignment.center,
                height: 60.0,
                decoration: new BoxDecoration(
                    color: Color(0xFF4364A1),
                    borderRadius: new BorderRadius.circular(9.0)),
                child: new Text("FaceBook",
                    style: new TextStyle(fontSize: 20.0, color: Colors.white))),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 20.0, top: 10.0),
            child: new Container(
                alignment: Alignment.center,
                height: 60.0,
                decoration: new BoxDecoration(
                    color: Color(0xFFDF513B),
                    borderRadius: new BorderRadius.circular(9.0)),
                child: new Text("Google",
                    style: new TextStyle(fontSize: 20.0, color: Colors.white))),
          ),
        )
      ],
    );
  }
}
