import 'package:construccion/includes/functions.dart';
import 'package:flutter/material.dart';


class Validator {

    Functions func = Functions();

    Function(String) numberValidator(bool isRequired) {
    Function(String) validator = (String value) {
      if (isRequired && value.isEmpty) {
        return "please enter a value";
      }
      if (!func.isNumeric(value)) {
        return "value must be a number";
      }
      return null;
    };
    return validator;
  }

  Function(String) emailValidator(bool isRequired) {
    Function(String) validator = (String value) {
      if (isRequired && value.isEmpty) {
        return "please enter a value";
      }
      return null;
    };
    return validator;
  }

    Function(String) passwordValidator(bool isRequired) {
    Function(String) validator = (String value) {
      if (isRequired && value.isEmpty) {
        return "please enter a value";
      }
      return null;
    };
    return validator;
  }

}