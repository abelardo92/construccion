import 'package:construccion/sales.dart';
import 'package:flutter/material.dart';
import 'package:construccion/login.dart';
import 'package:construccion/home.dart';

class Navigate {
  void toLoginPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LoginPage(),
      ),
    );
  }

  void toHomePage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => HomePage(),
      ),
    );
  }

  void toSalesPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SalesPage(),
      ),
    );
  }
}
