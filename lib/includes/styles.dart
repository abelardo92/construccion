import 'package:flutter/material.dart';

class Styles {

  static const primaryColor = Color(0xFF18D191);
  static const primaryIconColor = Colors.white;

  // Button attributes
  static const double buttonHeight = 50.0;

  ButtonThemeData defaultButtonThemeData() {
    return ButtonThemeData(
        minWidth: double.infinity,
        height: 50.0,
        buttonColor: primaryColor,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(5.0),
        ));
  }

  EdgeInsets logoPadding() {
    return EdgeInsets.only(top: 40.0, bottom: 80.0);
  }

  EdgeInsets textFieldPadding() {
    return EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0);
    // return EdgeInsets.only(top: 8.0, bottom: 80.0);
  }

  EdgeInsets buttonPadding() {
    return EdgeInsets.only(
      left: 20.0,
      right: 20.0,
      top: 10.0,
    );
  }

  static const EdgeInsets iconButtonPadding = const EdgeInsets.only(bottom: 2.5, right: 2.5);

  TextStyle buttonTextStyle() {
    return TextStyle(fontSize: 20, color: Colors.white);
  }

  TextStyle logoTextStyle() {
    return TextStyle(fontSize: 30, color: Colors.black87);
  }
}
