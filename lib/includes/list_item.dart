class ListItem {
  final String id;
  final String externalId;
  final String title;
  final String subtitle;

  ListItem(this.id, this.externalId, this.title, this.subtitle);
}