import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'sale.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'sale_service.jorm.dart';

// The model
class SaleService {
  SaleService();

  SaleService.make(
      this.id, this.externalId, this.name, this.createdAt, this.updatedAt);

  @PrimaryKey(auto: true)
  int id;

  @Column()
  int externalId;

  @Column()
  String name;

  @BelongsTo(SaleBean)
  int saleId;

  @Column()
  DateTime createdAt;

  @Column()
  DateTime updatedAt;

  String toString() =>
      'Product(id: $id, externalId: $externalId name: $name, createdAt: $createdAt, updatedAt: $updatedAt)';
}

@GenBean()
class SaleServiceBean extends Bean<SaleService> with _SaleServiceBean {

  SaleServiceBean(Adapter adapter) : super(adapter);

  Future<SaleService> findByExternalId(int externalId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.externalId.eq(externalId));
    return await findOne(find);
  }

  String get tableName => 'sale_products';

  SaleBean _saleBean;
  @override
  SaleBean get saleBean => _saleBean ??= new SaleBean(adapter);
}