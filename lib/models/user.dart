import 'dart:async';
import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'user.jorm.dart';

// The model
class User {
  User();

  User.make(this.id, this.name, this.birthDate, this.email, this.password,
      this.isActive, this.createdAt);

  @PrimaryKey(auto: true)
  int id;

  @Column()
  int externalId;

  @Column()
  String name;

  @Column(isNullable: true)
  DateTime birthDate;

  @Column()
  String email;

  @Column()
  String password;

  @Column()
  bool isActive;

  @Column()
  DateTime createdAt;

  @Column()
  DateTime updatedAt;

  String toString() =>
      'User(id: $id, name: $name, birthDate: $birthDate, email: $email, createdAt: $createdAt)';
}

@GenBean()
class UserBean extends Bean<User> with _UserBean {
  UserBean(Adapter adapter) : super(adapter);

  Future<User> findByExternalId(int externalId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.externalId.eq(externalId));
    return await findOne(find);
  }

  String get tableName => 'users';
}
