import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'sale.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'sale_product.jorm.dart';

// The model
class SaleProduct {
  SaleProduct();

  SaleProduct.make(
      this.id, this.externalId, this.name, this.createdAt, this.updatedAt);

  @PrimaryKey(auto: true)
  int id;

  @Column()
  int externalId;

  @Column()
  String name;

  @BelongsTo(SaleBean)
  int saleId;

  @Column()
  DateTime createdAt;

  @Column()
  DateTime updatedAt;

  String toString() =>
      'Product(id: $id, externalId: $externalId name: $name, createdAt: $createdAt, updatedAt: $updatedAt)';
}

@GenBean()
class SaleProductBean extends Bean<SaleProduct> with _SaleProductBean {

  SaleProductBean(Adapter adapter) : super(adapter);

  Future<SaleProduct> findByExternalId(int externalId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.externalId.eq(externalId));
    return await findOne(find);
  }

  String get tableName => 'sale_products';

  SaleBean _saleBean;
  @override
  SaleBean get saleBean => _saleBean ??= new SaleBean(adapter);
}