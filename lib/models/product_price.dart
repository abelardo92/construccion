import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'product.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'product_price.jorm.dart';

// The model
class ProductPrice {
  ProductPrice();

  ProductPrice.make(this.id, this.externalId, this.name, this.createdAt, this.updatedAt);

  @PrimaryKey(auto: true)
  int id;

  @Column()
  int externalId;

  @Column()
  String name;

  @BelongsTo(ProductBean)
  int productId;

  @Column(isNullable: true)
  DateTime createdAt;

  @Column(isNullable: true)
  DateTime updatedAt;

  String toString() =>
      'ProductPrice(id: $id, name: $name, createdAt: $createdAt)';
}

@GenBean()
class ProductPriceBean extends Bean<ProductPrice> with _ProductPriceBean {
  ProductPriceBean(Adapter adapter) : super(adapter);

  Future<ProductPrice> findByExternalId(int externalId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.externalId.eq(externalId));
    return await findOne(find);
  }

  String get tableName => 'product_price';

  ProductBean _productBean;
  @override
  // TODO: implement productBean
  ProductBean get productBean => _productBean ??= new ProductBean(adapter);
}