import 'package:construccion/includes/list_item.dart';
import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'customer.dart';
import 'sale_product.dart';
import 'sale_service.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'sale.jorm.dart';

// The model
class Sale {
  Sale();

  Sale.make(
      this.id, this.externalId, this.name, this.createdAt, this.updatedAt);

  @PrimaryKey(auto: true)
  int id;

  @Column()
  int externalId;

  @Column()
  String name;

  @BelongsTo(CustomerBean)
  int customerId;

  @HasMany(SaleProductBean)
  List<SaleProduct> saleProducts;

  @HasMany(SaleServiceBean)
  List<SaleService> saleServices;

  @Column()
  DateTime createdAt;

  @Column()
  DateTime updatedAt;

  // ProductPrice get currentPrice {
  //   if(productPrices != null && productPrices.length > 0) {
  //     return productPrices.last;
  //   }
  //   return null;
  // }

  ListItem toListItem() {
    return ListItem(this.id.toString(), this.externalId.toString(), this.name, toString());
  }

  String toString() =>
      'Sale(id: $id, externalId: $externalId name: $name, createdAt: $createdAt, updatedAt: $updatedAt)';
}

@GenBean()
class SaleBean extends Bean<Sale> with _SaleBean {
  SaleBean(Adapter adapter) : super(adapter);

  Future<Sale> findByExternalId(int externalId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.externalId.eq(externalId));
    return await findOne(find);
  }

  String get tableName => 'sales';

  CustomerBean _customerBean;
  SaleProductBean _saleProductBean;
  SaleServiceBean _saleServiceBean;

  @override
  CustomerBean get customerBean => _customerBean ??= new CustomerBean(adapter);

  @override
  SaleProductBean get saleProductBean =>
      _saleProductBean ??= new SaleProductBean(adapter);

  @override
  SaleServiceBean get saleServiceBean =>
      _saleServiceBean ??= new SaleServiceBean(adapter);
}
