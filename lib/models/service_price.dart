import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'service.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'service_price.jorm.dart';

// The model
class ServicePrice {
  ServicePrice();

  ServicePrice.make(this.id, this.externalId, this.name, this.createdAt, this.updatedAt);

  @PrimaryKey(auto: true)
  int id;

  @Column()
  int externalId;

  @Column()
  String name;

  @BelongsTo(ServiceBean)
  int serviceId;

  @Column(isNullable: true)
  DateTime createdAt;

  @Column(isNullable: true)
  DateTime updatedAt;

  String toString() =>
      'ServicePrice(id: $id, name: $name, createdAt: $createdAt)';
}

@GenBean()
class ServicePriceBean extends Bean<ServicePrice> with _ServicePriceBean {
  ServicePriceBean(Adapter adapter) : super(adapter);

  Future<ServicePrice> findByExternalId(int externalId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.externalId.eq(externalId));
    return await findOne(find);
  }

  String get tableName => 'service_price';

  ServiceBean _serviceBean;
  @override
  // TODO: implement ServiceBean
  ServiceBean get serviceBean => _serviceBean ??= new ServiceBean(adapter);

}