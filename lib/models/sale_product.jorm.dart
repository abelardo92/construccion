// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sale_product.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _SaleProductBean implements Bean<SaleProduct> {
  final id = IntField('id');
  final externalId = IntField('external_id');
  final name = StrField('name');
  final saleId = IntField('sale_id');
  final createdAt = DateTimeField('created_at');
  final updatedAt = DateTimeField('updated_at');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        id.name: id,
        externalId.name: externalId,
        name.name: name,
        saleId.name: saleId,
        createdAt.name: createdAt,
        updatedAt.name: updatedAt,
      };
  SaleProduct fromMap(Map map) {
    SaleProduct model = SaleProduct();
    model.id = adapter.parseValue(map['id']);
    model.externalId = adapter.parseValue(map['external_id']);
    model.name = adapter.parseValue(map['name']);
    model.saleId = adapter.parseValue(map['sale_id']);
    model.createdAt = adapter.parseValue(map['created_at']);
    model.updatedAt = adapter.parseValue(map['updated_at']);

    return model;
  }

  List<SetColumn> toSetColumns(SaleProduct model,
      {bool update = false, Set<String> only}) {
    List<SetColumn> ret = [];

    if (only == null) {
      if (model.id != null) {
        ret.add(id.set(model.id));
      }
      ret.add(externalId.set(model.externalId));
      ret.add(name.set(model.name));
      ret.add(saleId.set(model.saleId));
      ret.add(createdAt.set(model.createdAt));
      ret.add(updatedAt.set(model.updatedAt));
    } else {
      if (model.id != null) {
        if (only.contains(id.name)) ret.add(id.set(model.id));
      }
      if (only.contains(externalId.name))
        ret.add(externalId.set(model.externalId));
      if (only.contains(name.name)) ret.add(name.set(model.name));
      if (only.contains(saleId.name)) ret.add(saleId.set(model.saleId));
      if (only.contains(createdAt.name))
        ret.add(createdAt.set(model.createdAt));
      if (only.contains(updatedAt.name))
        ret.add(updatedAt.set(model.updatedAt));
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(id.name, primary: true, autoIncrement: true, isNullable: false);
    st.addInt(externalId.name, isNullable: false);
    st.addStr(name.name, isNullable: false);
    st.addInt(saleId.name,
        foreignTable: saleBean.tableName, foreignCol: 'id', isNullable: false);
    st.addDateTime(createdAt.name, isNullable: false);
    st.addDateTime(updatedAt.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(SaleProduct model, {bool cascade: false}) async {
    final Insert insert = inserter.setMany(toSetColumns(model)).id(id.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      SaleProduct newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<SaleProduct> models) async {
    final List<List<SetColumn>> data =
        models.map((model) => toSetColumns(model)).toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(SaleProduct model, {bool cascade: false}) async {
    final Upsert upsert = upserter.setMany(toSetColumns(model)).id(id.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      SaleProduct newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<SaleProduct> models) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(toSetColumns(model).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(SaleProduct model, {Set<String> only}) async {
    final Update update = updater
        .where(this.id.eq(model.id))
        .setMany(toSetColumns(model, only: only));
    return adapter.update(update);
  }

  Future<void> updateMany(List<SaleProduct> models) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(toSetColumns(model).toList());
      where.add(this.id.eq(model.id));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<SaleProduct> find(int id,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.id.eq(id));
    return await findOne(find);
  }

  Future<int> remove(int id) async {
    final Remove remove = remover.where(this.id.eq(id));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<SaleProduct> models) async {
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.id.eq(model.id));
    }
    return adapter.remove(remove);
  }

  Future<List<SaleProduct>> findBySale(int saleId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.saleId.eq(saleId));
    return findMany(find);
  }

  Future<List<SaleProduct>> findBySaleList(List<Sale> models,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder;
    for (Sale model in models) {
      find.or(this.saleId.eq(model.id));
    }
    return findMany(find);
  }

  Future<int> removeBySale(int saleId) async {
    final Remove rm = remover.where(this.saleId.eq(saleId));
    return await adapter.remove(rm);
  }

  void associateSale(SaleProduct child, Sale parent) {
    child.saleId = parent.id;
  }

  SaleBean get saleBean;
}
