import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'service_price.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'service.jorm.dart';

// The model
class Service {
  Service();

  Service.make(
      this.id, this.externalId, this.name, this.createdAt, this.updatedAt);

  @PrimaryKey(auto: true)
  int id;

  @Column()
  int externalId;

  @Column()
  String name;

  @HasMany(ServicePriceBean)
  List<ServicePrice> servicePrices;

  @Column()
  DateTime createdAt;

  @Column()
  DateTime updatedAt;

  ServicePrice get currentPrice {
    if(servicePrices != null && servicePrices.length > 0) {
      return servicePrices.last;
    }
    return null;
  }

  String toString() =>
      'Service(id: $id, externalId: $externalId name: $name, createdAt: $createdAt, updatedAt: $updatedAt)';
}

@GenBean()
class ServiceBean extends Bean<Service> with _ServiceBean {
  ServiceBean(Adapter adapter) : super(adapter);

  Future<Service> findByExternalId(int externalId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.externalId.eq(externalId));
    return await findOne(find);
  }

  String get tableName => 'services';
  
  ServicePriceBean _servicePriceBean;

  @override
  ServicePriceBean get servicePriceBean => _servicePriceBean ??= new ServicePriceBean(adapter);
}