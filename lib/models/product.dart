import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'product_price.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'product.jorm.dart';

// The model
class Product {
  Product();

  Product.make(
      this.id, this.externalId, this.name, this.createdAt, this.updatedAt);

  @PrimaryKey(auto: true)
  int id;

  @Column()
  int externalId;

  @Column()
  String name;

  @HasMany(ProductPriceBean)
  List<ProductPrice> productPrices;

  @Column()
  DateTime createdAt;

  @Column()
  DateTime updatedAt;

  ProductPrice get currentPrice {
    if(productPrices != null && productPrices.length > 0) {
      return productPrices.last;
    }
    return null;
  }

  String toString() =>
      'Product(id: $id, externalId: $externalId name: $name, createdAt: $createdAt, updatedAt: $updatedAt)';
}

@GenBean()
class ProductBean extends Bean<Product> with _ProductBean {
  ProductBean(Adapter adapter) : super(adapter);

  Future<Product> findByExternalId(int externalId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.externalId.eq(externalId));
    return await findOne(find);
  }

  String get tableName => 'products';
  
  ProductPriceBean _productPriceBean;
  @override
  // TODO: implement productPriceBean
  ProductPriceBean get productPriceBean => _productPriceBean ??= new ProductPriceBean(adapter);
}
