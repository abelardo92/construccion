// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sale.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _SaleBean implements Bean<Sale> {
  final id = IntField('id');
  final externalId = IntField('external_id');
  final name = StrField('name');
  final customerId = IntField('customer_id');
  final createdAt = DateTimeField('created_at');
  final updatedAt = DateTimeField('updated_at');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        id.name: id,
        externalId.name: externalId,
        name.name: name,
        customerId.name: customerId,
        createdAt.name: createdAt,
        updatedAt.name: updatedAt,
      };
  Sale fromMap(Map map) {
    Sale model = Sale();
    model.id = adapter.parseValue(map['id']);
    model.externalId = adapter.parseValue(map['external_id']);
    model.name = adapter.parseValue(map['name']);
    model.customerId = adapter.parseValue(map['customer_id']);
    model.createdAt = adapter.parseValue(map['created_at']);
    model.updatedAt = adapter.parseValue(map['updated_at']);

    return model;
  }

  List<SetColumn> toSetColumns(Sale model,
      {bool update = false, Set<String> only}) {
    List<SetColumn> ret = [];

    if (only == null) {
      if (model.id != null) {
        ret.add(id.set(model.id));
      }
      ret.add(externalId.set(model.externalId));
      ret.add(name.set(model.name));
      ret.add(customerId.set(model.customerId));
      ret.add(createdAt.set(model.createdAt));
      ret.add(updatedAt.set(model.updatedAt));
    } else {
      if (model.id != null) {
        if (only.contains(id.name)) ret.add(id.set(model.id));
      }
      if (only.contains(externalId.name))
        ret.add(externalId.set(model.externalId));
      if (only.contains(name.name)) ret.add(name.set(model.name));
      if (only.contains(customerId.name))
        ret.add(customerId.set(model.customerId));
      if (only.contains(createdAt.name))
        ret.add(createdAt.set(model.createdAt));
      if (only.contains(updatedAt.name))
        ret.add(updatedAt.set(model.updatedAt));
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(id.name, primary: true, autoIncrement: true, isNullable: false);
    st.addInt(externalId.name, isNullable: false);
    st.addStr(name.name, isNullable: false);
    st.addInt(customerId.name,
        foreignTable: customerBean.tableName,
        foreignCol: 'id',
        isNullable: false);
    st.addDateTime(createdAt.name, isNullable: false);
    st.addDateTime(updatedAt.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Sale model, {bool cascade: false}) async {
    final Insert insert = inserter.setMany(toSetColumns(model)).id(id.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      Sale newModel;
      if (model.saleProducts != null) {
        newModel ??= await find(retId);
        model.saleProducts
            .forEach((x) => saleProductBean.associateSale(x, newModel));
        for (final child in model.saleProducts) {
          await saleProductBean.insert(child);
        }
      }
      if (model.saleServices != null) {
        newModel ??= await find(retId);
        model.saleServices
            .forEach((x) => saleServiceBean.associateSale(x, newModel));
        for (final child in model.saleServices) {
          await saleServiceBean.insert(child);
        }
      }
    }
    return retId;
  }

  Future<void> insertMany(List<Sale> models, {bool cascade: false}) async {
    if (cascade) {
      final List<Future> futures = [];
      for (var model in models) {
        futures.add(insert(model, cascade: cascade));
      }
      await Future.wait(futures);
      return;
    } else {
      final List<List<SetColumn>> data =
          models.map((model) => toSetColumns(model)).toList();
      final InsertMany insert = inserters.addAll(data);
      await adapter.insertMany(insert);
      return;
    }
  }

  Future<dynamic> upsert(Sale model, {bool cascade: false}) async {
    final Upsert upsert = upserter.setMany(toSetColumns(model)).id(id.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      Sale newModel;
      if (model.saleProducts != null) {
        newModel ??= await find(retId);
        model.saleProducts
            .forEach((x) => saleProductBean.associateSale(x, newModel));
        for (final child in model.saleProducts) {
          await saleProductBean.upsert(child);
        }
      }
      if (model.saleServices != null) {
        newModel ??= await find(retId);
        model.saleServices
            .forEach((x) => saleServiceBean.associateSale(x, newModel));
        for (final child in model.saleServices) {
          await saleServiceBean.upsert(child);
        }
      }
    }
    return retId;
  }

  Future<void> upsertMany(List<Sale> models, {bool cascade: false}) async {
    if (cascade) {
      final List<Future> futures = [];
      for (var model in models) {
        futures.add(upsert(model, cascade: cascade));
      }
      await Future.wait(futures);
      return;
    } else {
      final List<List<SetColumn>> data = [];
      for (var i = 0; i < models.length; ++i) {
        var model = models[i];
        data.add(toSetColumns(model).toList());
      }
      final UpsertMany upsert = upserters.addAll(data);
      await adapter.upsertMany(upsert);
      return;
    }
  }

  Future<int> update(Sale model,
      {bool cascade: false, bool associate: false, Set<String> only}) async {
    final Update update = updater
        .where(this.id.eq(model.id))
        .setMany(toSetColumns(model, only: only));
    final ret = adapter.update(update);
    if (cascade) {
      Sale newModel;
      if (model.saleProducts != null) {
        if (associate) {
          newModel ??= await find(model.id);
          model.saleProducts
              .forEach((x) => saleProductBean.associateSale(x, newModel));
        }
        for (final child in model.saleProducts) {
          await saleProductBean.update(child);
        }
      }
      if (model.saleServices != null) {
        if (associate) {
          newModel ??= await find(model.id);
          model.saleServices
              .forEach((x) => saleServiceBean.associateSale(x, newModel));
        }
        for (final child in model.saleServices) {
          await saleServiceBean.update(child);
        }
      }
    }
    return ret;
  }

  Future<void> updateMany(List<Sale> models, {bool cascade: false}) async {
    if (cascade) {
      final List<Future> futures = [];
      for (var model in models) {
        futures.add(update(model, cascade: cascade));
      }
      await Future.wait(futures);
      return;
    } else {
      final List<List<SetColumn>> data = [];
      final List<Expression> where = [];
      for (var i = 0; i < models.length; ++i) {
        var model = models[i];
        data.add(toSetColumns(model).toList());
        where.add(this.id.eq(model.id));
      }
      final UpdateMany update = updaters.addAll(data, where);
      await adapter.updateMany(update);
      return;
    }
  }

  Future<Sale> find(int id, {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.id.eq(id));
    final Sale model = await findOne(find);
    if (preload && model != null) {
      await this.preload(model, cascade: cascade);
    }
    return model;
  }

  Future<int> remove(int id, [bool cascade = false]) async {
    if (cascade) {
      final Sale newModel = await find(id);
      if (newModel != null) {
        await saleProductBean.removeBySale(newModel.id);
        await saleServiceBean.removeBySale(newModel.id);
      }
    }
    final Remove remove = remover.where(this.id.eq(id));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Sale> models) async {
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.id.eq(model.id));
    }
    return adapter.remove(remove);
  }

  Future<List<Sale>> findByCustomer(int customerId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.customerId.eq(customerId));
    final List<Sale> models = await findMany(find);
    if (preload) {
      await this.preloadAll(models, cascade: cascade);
    }
    return models;
  }

  Future<List<Sale>> findByCustomerList(List<Customer> models,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder;
    for (Customer model in models) {
      find.or(this.customerId.eq(model.id));
    }
    final List<Sale> retModels = await findMany(find);
    if (preload) {
      await this.preloadAll(retModels, cascade: cascade);
    }
    return retModels;
  }

  Future<int> removeByCustomer(int customerId) async {
    final Remove rm = remover.where(this.customerId.eq(customerId));
    return await adapter.remove(rm);
  }

  void associateCustomer(Sale child, Customer parent) {
    child.customerId = parent.id;
  }

  Future<Sale> preload(Sale model, {bool cascade: false}) async {
    model.saleProducts = await saleProductBean.findBySale(model.id,
        preload: cascade, cascade: cascade);
    model.saleServices = await saleServiceBean.findBySale(model.id,
        preload: cascade, cascade: cascade);
    return model;
  }

  Future<List<Sale>> preloadAll(List<Sale> models,
      {bool cascade: false}) async {
    models.forEach((Sale model) => model.saleProducts ??= []);
    await OneToXHelper.preloadAll<Sale, SaleProduct>(
        models,
        (Sale model) => [model.id],
        saleProductBean.findBySaleList,
        (SaleProduct model) => [model.saleId],
        (Sale model, SaleProduct child) => model.saleProducts.add(child),
        cascade: cascade);
    models.forEach((Sale model) => model.saleServices ??= []);
    await OneToXHelper.preloadAll<Sale, SaleService>(
        models,
        (Sale model) => [model.id],
        saleServiceBean.findBySaleList,
        (SaleService model) => [model.saleId],
        (Sale model, SaleService child) => model.saleServices.add(child),
        cascade: cascade);
    return models;
  }

  SaleProductBean get saleProductBean;
  SaleServiceBean get saleServiceBean;
  CustomerBean get customerBean;
}
