import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'sale.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'customer.jorm.dart';

// The model
class Customer {
  Customer();

  Customer.make(
      this.id, this.externalId, this.name, this.createdAt, this.updatedAt);

  @PrimaryKey(auto: true)
  int id;

  @Column()
  int externalId;

  @Column()
  String name;

  @HasMany(SaleBean)
  List<Sale> sales;

  @Column()
  DateTime createdAt;

  @Column()
  DateTime updatedAt;

  String toString() =>
      'Product(id: $id, externalId: $externalId name: $name, createdAt: $createdAt, updatedAt: $updatedAt)';
}

@GenBean()
class CustomerBean extends Bean<Customer> with _CustomerBean {

  CustomerBean(Adapter adapter) : super(adapter);

  Future<Customer> findByExternalId(int externalId,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.externalId.eq(externalId));
    return await findOne(find);
  }

  String get tableName => 'customers';

  SaleBean _saleBean;
  @override
  SaleBean get saleBean => _saleBean ??= new SaleBean(adapter);
}