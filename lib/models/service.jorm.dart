// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _ServiceBean implements Bean<Service> {
  final id = IntField('id');
  final externalId = IntField('external_id');
  final name = StrField('name');
  final createdAt = DateTimeField('created_at');
  final updatedAt = DateTimeField('updated_at');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        id.name: id,
        externalId.name: externalId,
        name.name: name,
        createdAt.name: createdAt,
        updatedAt.name: updatedAt,
      };
  Service fromMap(Map map) {
    Service model = Service();
    model.id = adapter.parseValue(map['id']);
    model.externalId = adapter.parseValue(map['external_id']);
    model.name = adapter.parseValue(map['name']);
    model.createdAt = adapter.parseValue(map['created_at']);
    model.updatedAt = adapter.parseValue(map['updated_at']);

    return model;
  }

  List<SetColumn> toSetColumns(Service model,
      {bool update = false, Set<String> only}) {
    List<SetColumn> ret = [];

    if (only == null) {
      if (model.id != null) {
        ret.add(id.set(model.id));
      }
      ret.add(externalId.set(model.externalId));
      ret.add(name.set(model.name));
      ret.add(createdAt.set(model.createdAt));
      ret.add(updatedAt.set(model.updatedAt));
    } else {
      if (model.id != null) {
        if (only.contains(id.name)) ret.add(id.set(model.id));
      }
      if (only.contains(externalId.name))
        ret.add(externalId.set(model.externalId));
      if (only.contains(name.name)) ret.add(name.set(model.name));
      if (only.contains(createdAt.name))
        ret.add(createdAt.set(model.createdAt));
      if (only.contains(updatedAt.name))
        ret.add(updatedAt.set(model.updatedAt));
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(id.name, primary: true, autoIncrement: true, isNullable: false);
    st.addInt(externalId.name, isNullable: false);
    st.addStr(name.name, isNullable: false);
    st.addDateTime(createdAt.name, isNullable: false);
    st.addDateTime(updatedAt.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Service model, {bool cascade: false}) async {
    final Insert insert = inserter.setMany(toSetColumns(model)).id(id.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      Service newModel;
      if (model.servicePrices != null) {
        newModel ??= await find(retId);
        model.servicePrices
            .forEach((x) => servicePriceBean.associateService(x, newModel));
        for (final child in model.servicePrices) {
          await servicePriceBean.insert(child);
        }
      }
    }
    return retId;
  }

  Future<void> insertMany(List<Service> models, {bool cascade: false}) async {
    if (cascade) {
      final List<Future> futures = [];
      for (var model in models) {
        futures.add(insert(model, cascade: cascade));
      }
      await Future.wait(futures);
      return;
    } else {
      final List<List<SetColumn>> data =
          models.map((model) => toSetColumns(model)).toList();
      final InsertMany insert = inserters.addAll(data);
      await adapter.insertMany(insert);
      return;
    }
  }

  Future<dynamic> upsert(Service model, {bool cascade: false}) async {
    final Upsert upsert = upserter.setMany(toSetColumns(model)).id(id.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      Service newModel;
      if (model.servicePrices != null) {
        newModel ??= await find(retId);
        model.servicePrices
            .forEach((x) => servicePriceBean.associateService(x, newModel));
        for (final child in model.servicePrices) {
          await servicePriceBean.upsert(child);
        }
      }
    }
    return retId;
  }

  Future<void> upsertMany(List<Service> models, {bool cascade: false}) async {
    if (cascade) {
      final List<Future> futures = [];
      for (var model in models) {
        futures.add(upsert(model, cascade: cascade));
      }
      await Future.wait(futures);
      return;
    } else {
      final List<List<SetColumn>> data = [];
      for (var i = 0; i < models.length; ++i) {
        var model = models[i];
        data.add(toSetColumns(model).toList());
      }
      final UpsertMany upsert = upserters.addAll(data);
      await adapter.upsertMany(upsert);
      return;
    }
  }

  Future<int> update(Service model,
      {bool cascade: false, bool associate: false, Set<String> only}) async {
    final Update update = updater
        .where(this.id.eq(model.id))
        .setMany(toSetColumns(model, only: only));
    final ret = adapter.update(update);
    if (cascade) {
      Service newModel;
      if (model.servicePrices != null) {
        if (associate) {
          newModel ??= await find(model.id);
          model.servicePrices
              .forEach((x) => servicePriceBean.associateService(x, newModel));
        }
        for (final child in model.servicePrices) {
          await servicePriceBean.update(child);
        }
      }
    }
    return ret;
  }

  Future<void> updateMany(List<Service> models, {bool cascade: false}) async {
    if (cascade) {
      final List<Future> futures = [];
      for (var model in models) {
        futures.add(update(model, cascade: cascade));
      }
      await Future.wait(futures);
      return;
    } else {
      final List<List<SetColumn>> data = [];
      final List<Expression> where = [];
      for (var i = 0; i < models.length; ++i) {
        var model = models[i];
        data.add(toSetColumns(model).toList());
        where.add(this.id.eq(model.id));
      }
      final UpdateMany update = updaters.addAll(data, where);
      await adapter.updateMany(update);
      return;
    }
  }

  Future<Service> find(int id,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.id.eq(id));
    final Service model = await findOne(find);
    if (preload && model != null) {
      await this.preload(model, cascade: cascade);
    }
    return model;
  }

  Future<int> remove(int id, [bool cascade = false]) async {
    if (cascade) {
      final Service newModel = await find(id);
      if (newModel != null) {
        await servicePriceBean.removeByService(newModel.id);
      }
    }
    final Remove remove = remover.where(this.id.eq(id));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Service> models) async {
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.id.eq(model.id));
    }
    return adapter.remove(remove);
  }

  Future<Service> preload(Service model, {bool cascade: false}) async {
    model.servicePrices = await servicePriceBean.findByService(model.id,
        preload: cascade, cascade: cascade);
    return model;
  }

  Future<List<Service>> preloadAll(List<Service> models,
      {bool cascade: false}) async {
    models.forEach((Service model) => model.servicePrices ??= []);
    await OneToXHelper.preloadAll<Service, ServicePrice>(
        models,
        (Service model) => [model.id],
        servicePriceBean.findByServiceList,
        (ServicePrice model) => [model.serviceId],
        (Service model, ServicePrice child) => model.servicePrices.add(child),
        cascade: cascade);
    return models;
  }

  ServicePriceBean get servicePriceBean;
}
